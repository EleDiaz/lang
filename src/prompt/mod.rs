use std::process::Command;

//use clap;

use termion::{color, style};

use config::Config;
use interpreter::Interpreter;

const DEFAULT_PROMPT: &'static str = ">>> ";

type Replacer = fn(&str) -> String;

pub struct Prompt {
    cfg_prompt: String,
}


impl Prompt {
    pub fn new(config: &Config) -> Self {
        let cfg_prompt = config.get_config()["repl"]["prompt"]
            .as_str()
            .unwrap_or(">>> ")
            .to_owned();

        Self { cfg_prompt }
    }

    pub fn get_prompt(&self, _interpreter: &Interpreter) -> String {
        // TODO: cambiar la shell segun so
        let shell_replacer: Replacer = |shell_cmd: &str| -> String {
            Command::new("sh")
                .arg("-c")
                .arg(shell_cmd)
                .output()
                .map(|output| if output.status.success() {
                    String::from(String::from_utf8_lossy(&output.stdout).trim_right())
                } else {
                    format!(
                        "{}{}{}",
                        color::Bg(color::Red),
                        String::from_utf8_lossy(&output.stderr),
                        style::Reset
                    )
                })
                .unwrap_or(format!(
                    "{}{}{}",
                    color::Bg(color::Red),
                    shell_cmd,
                    style::Reset
                ))
        };

        // TODO: Añadir el tema de los modulos
        let repl_replacer: Replacer = |repl_cmd: &str| -> String {
            match repl_cmd {
                "version" => format!("{}", crate_version!()),
                _ => format!("{}{}{}", color::Bg(color::Red), repl_cmd, style::Reset),
            }
        };

        grammar::parse_prompt(&self.cfg_prompt, shell_replacer, repl_replacer)
            .unwrap_or(DEFAULT_PROMPT.to_owned())
    }
    // TODO: save prompt configuration
}

pub mod grammar {
    include!(concat!(env!("OUT_DIR"), "/grammar_prompt.rs"));
}
