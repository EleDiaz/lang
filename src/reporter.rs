//use termion::{color, style};

use error::IError;


pub struct Reporter {}

impl Reporter {
    pub fn new() -> Self {
        Self {}
    }

    pub fn report_ierror(&self, ierror: &IError) {
        println!("{}", ierror.pretty_error());
    }

    pub fn report(&self, message: &str) {
        println!("{}", message);
    }
}
