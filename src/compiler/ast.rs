use compiler::layer::intermediate::Instruction;
use compiler::layer::scope::Scope;
use compiler::layer::transform_program;
use compiler::parser::TokenInfo;
use error;
use fileref::FileRef;
use reftable::RefTable;
use reporter::Reporter;
use value::Value;

pub type NameId = String;

#[derive(Debug, PartialEq, Clone)]
pub enum Interpreter {
    Command(NameId, Vec<NameId>),
    Code(Program<TokenInfo>),
}

pub type Accesor = Vec<NameId>;

#[derive(Debug, PartialEq, Clone, Eq)]
pub enum Program<T> {
    Import(Accesor, T),
    Module(NameId, Box<Program<T>>, T),
    Expression(Expression<T>, T),
}

impl Program<TokenInfo> {
    pub fn evaluate<'a>(
        self,
        fileref: &FileRef,
        reftable: &mut RefTable<Value>,
        scope: &mut Scope,
        reporter: &Reporter,
    ) -> (Option<Value>, Vec<error::IError>) {
        match self {
            Program::Expression(expr, _t) => expr.evaluate(fileref, reftable, scope, &reporter),
            _ => unimplemented!(),
        }
    }
}


// TODO: Add Breadcrumbs types to remove duplicate or inecessary data
// pub enum BreadcrumbsTypes {
//     If,
//     Else,
//     Main,
//     Module(usize),
//     For,
//     Fun,
// }

// pub struct Breadcrumbs {
//     path: Vec<BreadcrumbsTypes>,
// }

pub type Expression<T> = ExpressionG<NameId, T>;

#[derive(Debug, PartialEq, Clone, Eq)]
pub enum ExpressionG<Name, T> {
    SequenceExpr(Vec<ExpressionG<Name, T>>, T),
    FunDecl(Vec<Name>, Box<ExpressionG<Name, T>>, T),
    VarDecl(Name, Box<ExpressionG<Name, T>>, T),
    CommandDecl(String, T),
    If(Box<ExpressionG<Name, T>>, Box<ExpressionG<Name, T>>, T),
    IfElse(Box<ExpressionG<Name, T>>, Box<ExpressionG<Name, T>>, Box<ExpressionG<Name, T>>, T),
    For(Option<Name>, Box<ExpressionG<Name, T>>, Box<ExpressionG<Name, T>>, T),
    Apply(Name, Vec<ExpressionG<Name, T>>, T),
    Identifier(Name, T),
    Factor(Atom, T),
}


impl ExpressionG<NameId, TokenInfo> {
    pub fn evaluate(
        self,
        fileref: &FileRef,
        mut reftable: &mut RefTable<Value>,
        mut scope: &mut Scope,
        reporter: &Reporter,
    ) -> (Option<Value>, Vec<error::IError>) {
        let (instrs, _errors) = transform_program(self, reporter, &mut scope);
        let opt_value = instrs.and_then(|instrs| {
            Instruction::eval(&instrs, &mut reftable)
                .and_then(|value| value.extract(&mut reftable))
                .ok()
        });

        (opt_value, Vec::new())
    }
}

#[derive(Debug, PartialEq, Clone, Eq)]
pub enum Atom {
    Number(i32),
    String(String),
    Boolean(bool),
    Vector(Vec<Atom>),
    Mapping(Vec<(String, Atom)>),
}

impl Atom {
    pub fn to_values(&self) -> Value {
        match self {
            &Atom::String(ref string) => Value::String(string.to_owned()),
            &Atom::Number(num) => Value::Number(num),
            &Atom::Boolean(boolean) => Value::Boolean(boolean),
            &Atom::Vector(ref values) => {
                Value::Vec(values.iter().map(|val| val.to_values()).collect())
            }
            &Atom::Mapping(ref values) => Value::Mapping(
                values
                    .iter()
                    .map(|&(ref key, ref val)| (key.to_owned(), val.to_values()))
                    .collect(),
            ),
        }
    }
}
