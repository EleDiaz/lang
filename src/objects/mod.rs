use std::collections::HashMap;
use std::env;
use std::path::PathBuf;

use value::Value;

pub type RustCall = fn(Vec<Value>) -> Value;

macro_rules! mk_natives {
    ($($name:expr => $call:expr),*) => {
        {
            let mut aux_map = HashMap::new();
            $(aux_map.insert($name.to_owned(), Value::Native($call));)*
            aux_map
        }
    }
}

/*
Hay que ver las capacidades del manejo de AST en rust quizas es necesario
usar macros 2.0
macro_rules! mk_typed_function {
    (($($type:ident),*) => $fun:expr) => {
        {
            |vec_values| => {
                fun()
            }
            fun()
        }
    }
}
*/

pub fn native_funtions() -> HashMap<String, Value> {
    mk_natives![
        "cd" => cd_fun,
        "pwd" => pwd_fun,
        "debug" => debug_fun,
        "+" => plus_fun
    ]
}

fn pwd_fun(args: Vec<Value>) -> Value {
    if args.len() > 0 {
        println!("Error pwd command doesn't take parameters");
        Value::Nil
    } else {
        if let Ok(val) = env::current_dir() {
            Value::String(val.to_str().unwrap().to_owned())
        } else {
            println!("Error getting current dir");
            Value::Nil
        }
    }
}
fn cd_fun(args: Vec<Value>) -> Value {
    args.get(0).map(|val| match val {
        &Value::String(ref string) => {
            let _ = env::set_current_dir(PathBuf::from(string.clone()));
        }
        _ => {}
    });

    // TODO:
    Value::Nil
}

fn plus_fun(args: Vec<Value>) -> Value {
    if args.len() != 2 {
        Value::Nil
    } else {
        let (num1, num2) = args.split_at(1);
        match (&num1[0], &num2[0]) {
            (&Value::Number(val1), &Value::Number(val2)) => Value::Number(val1 + val2),
            _ => Value::Nil,
        }
    }
}

fn debug_fun(args: Vec<Value>) -> Value {
    for value in args.iter() {
        println!("{}", value);
    }
    Value::Nil
}
