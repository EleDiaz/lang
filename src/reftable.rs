use std::collections::HashMap;

use module::Module;

#[derive(Debug)]
struct InfoVar<V> {
    count: usize,
    value: V,
}

impl<V> InfoVar<V> {
    pub fn new(value: V) -> Self {
        Self { count: 1, value }
    }
}

/// TODO: Hay que comprobar la ventaja con un intmap
#[derive(Debug)]
pub struct RefTable<V: Clone> {
    table: HashMap<usize, InfoVar<V>>,
}

impl<V: Clone> RefTable<V> {
    pub fn new() -> Self {
        Self { table: HashMap::new() }
    }

    pub fn from_modules(_module: &Vec<Module>) -> Self {
        Self::new()
    }

    pub fn set_var(&mut self, id: usize, value: V) {
        self.table
            .entry(id)
            .and_modify(|info| info.value = value.clone())
            .or_insert(InfoVar::new(value));
    }

    pub fn get_var(&mut self, id: usize) -> Option<V> {
        self.table.get_mut(&id).map(|info| {
            info.count += 1;
            info.value.clone()
        })
    }

    /// TODO: Ver si es posible evitar llevar count con el modify
    pub fn modify_var<F: FnOnce(&V) -> ()>(&mut self, id: usize, func: F) -> bool {
        self.table
            .get_mut(&id)
            .map(|info| func(&mut info.value))
            .is_some()
    }

    pub fn drop_var(&mut self, id: usize) -> bool {
        self.table.remove(&id).is_some()
    }
}
