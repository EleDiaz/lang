use futures::{Future, Stream};
use hyper;
use hyper::{Client, Method, Request, Uri};
use hyper::client::HttpConnector;
use hyper::header::{Authorization, Basic, ContentLength, ContentType, Scheme, UserAgent};
use hyper_tls::HttpsConnector;
use serde_json;
use serde_json::Value;
use tokio_core::reactor::Core;


#[derive(Debug)]
pub struct Auth {
    core: Core,
    client: Client<HttpsConnector<HttpConnector>>,
    auth_url: Uri,
    body: String,
    user_agent: String,
}


// TODO: Posiblemente halla que implementar un constructor a partir de un yaml
impl Auth {
    /// Initialize a connection with parameters given
    pub fn new(
        auth_url: String,
        body: String,
        user_agent: String,
    ) -> Result<Self, hyper::error::UriError> {
        let core = Core::new().unwrap();
        let client = Client::configure()
            .connector(HttpsConnector::new(1, &core.handle()).unwrap())
            .build(&core.handle());

        Ok(Self {
            core,
            client,
            auth_url: auth_url.parse()?,
            body,
            user_agent,
        })
    }

    pub fn auth<U, P>(&mut self, username: U, password: P) -> serde_json::Result<Value>
    where
        P: FnOnce(&str) -> String,
        U: FnOnce(&str) -> String,
    {

        let auth = Basic {
            username: username("Username"),
            password: Some(password("password")),
        };

        self.send(auth)
    }

    fn send<T>(&mut self, auth: T) -> serde_json::Result<Value>
    where
        T: 'static + Scheme + Clone,
    {
        let mut req = Request::new(Method::Post, self.auth_url.clone());
        req.headers_mut().set(
            UserAgent::new(self.user_agent.clone()),
        );
        req.headers_mut().set(ContentType::json());
        req.headers_mut().set(ContentLength(self.body.len() as u64));
        req.set_body(self.body.to_owned().clone());
        let auth2 = Authorization(auth);
        req.headers_mut().set(auth2);

        let post = self.client.request(req).and_then(
            |res| res.body().concat2(),
        );
        let res = self.core.run(post).unwrap();
        serde_json::from_slice(&res)
    }
}
